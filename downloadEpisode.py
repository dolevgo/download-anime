from urllib import request

def downloadProgress(filename, percentage):
        print(f'{filename}: {percentage}%')

class Downloader:
	def __init__(self, href, filename, full_path, hook=None):
		self.href = href
		self.filename = filename
		self.full_path = full_path
		self.percentage = 0
		if hook:
			self.hook = hook
		else:
			self.hook = downloadProgress
	
	def start(self):
                print('Downloading ' + self.filename)
                request.urlretrieve(self.href, self.full_path, reporthook=self.downloadMiddleware)
                print('Finished downloading ' + self.filename)
		
	def downloadMiddleware(self, count, blockSize, totalSize):
		percentage = int(count * blockSize * 100 / totalSize) 
		if self.percentage != percentage:
			self.percentage = percentage
			if self.hook:
				self.hook(self.filename, self.percentage)
		

